package com.rainist.app.ui.main.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rainist.app.core.model.Repo
import com.rainist.app.data.GithubRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailsViewModel @Inject constructor(
    private val repository: GithubRepository
) : ViewModel() {

    val repo = MutableLiveData<Repo>()

    private val compositeDisposable = CompositeDisposable()

    fun getRepo(repoId: Long) {
        val disposable = repository.getRepo(repoId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ repo.value = it }, { it.printStackTrace() })
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}