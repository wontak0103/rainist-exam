package com.rainist.app

import com.rainist.app.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class MobileApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<MobileApplication> {
        return DaggerAppComponent.builder()
            .create(this)
    }
}