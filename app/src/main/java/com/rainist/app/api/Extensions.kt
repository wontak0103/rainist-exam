package com.rainist.app.api

import com.rainist.app.api.model.Repo
import com.rainist.app.api.model.User

fun Repo.toCoreModel() = com.rainist.app.core.model.Repo(id, name, desc, stars, this.owner.id, this.owner.avatarUrl)

fun User.toCoreModel() = com.rainist.app.core.model.User(id, name, avatarUrl, publicRepos)