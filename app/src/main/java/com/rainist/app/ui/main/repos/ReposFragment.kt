package com.rainist.app.ui.main.repos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.rainist.app.R
import com.rainist.app.ui.base.BaseFragment
import kotlinx.android.synthetic.main.repos_fragment.*

class ReposFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.repos_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view_pager.adapter = ReposPagerAdapter(arguments?.getString(KEY_USERNAME)!!, fragmentManager!!)
        tab_layout.setupWithViewPager(view_pager)
    }

    companion object {

        const val KEY_USERNAME = "username"

        fun newInstance(username: String): ReposFragment {
            return ReposFragment().apply {
                arguments = bundleOf(Pair(KEY_USERNAME, username))
            }
        }
    }
}