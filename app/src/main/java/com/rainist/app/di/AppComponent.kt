package com.rainist.app.di

import com.rainist.app.MobileApplication
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        ActivityBindingModule::class
    ]
)
interface AppComponent : AndroidInjector<MobileApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<MobileApplication>()
}