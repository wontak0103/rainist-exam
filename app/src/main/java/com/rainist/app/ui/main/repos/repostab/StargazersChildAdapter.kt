package com.rainist.app.ui.main.repos.repostab

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.rainist.app.R
import com.rainist.app.core.model.User
import com.rainist.app.ui.main.repos.OnItemCallback

class StargazersChildAdapter(
    private val callback: OnItemCallback
) : ListAdapter<User, StargazerViewHolder>(COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StargazerViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.stargazer_horizontal_view_item, parent, false)
        return StargazerViewHolder(view)
    }

    override fun onBindViewHolder(holder: StargazerViewHolder, position: Int) {
        val stargazer = getItem(position)
        holder.bind(stargazer) {
            val user = getItem(it)
            callback.onItemClicked(user.avatarUrl, user.name)
        }
    }

    companion object {

        private val COMPARATOR = object : DiffUtil.ItemCallback<User>() {
            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean =
                oldItem == newItem
        }
    }
}