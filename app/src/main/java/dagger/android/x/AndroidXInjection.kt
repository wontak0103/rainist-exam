package dagger.android.x

import android.util.Log
import androidx.fragment.app.Fragment
import dagger.internal.Beta
import dagger.internal.Preconditions.checkNotNull

@Beta
object AndroidXInjection {

    fun inject(fragment: Fragment) {
        checkNotNull(fragment, "fragment")
        val hasFragmentXInjector = findHasFragmentInjector(fragment)
        Log.d(
            "AndroidXInjection",
            "An injector for ${fragment.javaClass.canonicalName} was found in ${hasFragmentXInjector.javaClass.canonicalName}"
        )

        val fragmentInjector = hasFragmentXInjector.fragmentXInjector()
        checkNotNull(
            fragmentInjector,
            "%s.fragmentXInjector() returned null",
            hasFragmentXInjector.javaClass
        )

        fragmentInjector.inject(fragment)
    }

    private fun findHasFragmentInjector(fragment: Fragment): HasFragmentXInjector {
        var parentFragment: Fragment? = fragment.parentFragment
        while (parentFragment != null) {
            if (parentFragment is HasFragmentXInjector) {
                return parentFragment
            }

            parentFragment = parentFragment.parentFragment
        }

        val activity = fragment.activity
        if (activity is HasFragmentXInjector) {
            return activity
        }

        if (activity?.application is HasFragmentXInjector) {
            return activity.application as HasFragmentXInjector
        }

        throw IllegalArgumentException(String.format("No injector was found for %s", fragment.javaClass.canonicalName))
    }
}