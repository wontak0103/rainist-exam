package com.rainist.app.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rainist.app.core.model.User
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(users: List<User>): Completable

    @Query("SELECT * FROM users WHERE id = :id LIMIT 1")
    fun user(id: Long): Single<User>

    @Query("SELECT * FROM users WHERE id IN (:ids)")
    fun users(ids: List<Long>): Single<List<User>>

    @Query("SELECT * FROM users WHERE name = :name LIMIT 1")
    fun findUserByName(name: String): Single<List<User>>
}
