package com.rainist.app.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rainist.app.core.model.Repo
import com.rainist.app.core.model.Star
import com.rainist.app.core.model.User
import com.rainist.app.db.dao.RepoDao
import com.rainist.app.db.dao.StarDao
import com.rainist.app.db.dao.UserDao

@Database(
    entities = [
        Repo::class,
        User::class,
        Star::class
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun reposDao(): RepoDao

    abstract fun userDao(): UserDao

    abstract fun stargazerDao(): StarDao
}