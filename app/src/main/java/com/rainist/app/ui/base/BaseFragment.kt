package com.rainist.app.ui.base

import dagger.android.x.DaggerFragment

abstract class BaseFragment : DaggerFragment()