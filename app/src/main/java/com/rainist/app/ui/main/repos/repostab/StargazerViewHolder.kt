package com.rainist.app.ui.main.repos.repostab

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rainist.app.R
import com.rainist.app.core.model.User
import com.squareup.picasso.Picasso

class StargazerViewHolder(
    view: View
) : RecyclerView.ViewHolder(view) {

    private val image: ImageView = view.findViewById(R.id.image)
    private val nameLabel: TextView = view.findViewById(R.id.label_name)

    private var stargazer: User? = null

    fun bind(stargazer: User?, onClick: (position: Int) -> Unit) {
        itemView.setOnClickListener {
            onClick(adapterPosition)
        }
        if (stargazer != null) {
            this.stargazer = stargazer
            Picasso.get()
                .load(stargazer.avatarUrl)
                .into(image)
            nameLabel.text = stargazer.name
        }
    }
}