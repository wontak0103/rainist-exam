package com.rainist.app.ui.main.repos.repostab

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.rainist.app.R
import com.rainist.app.core.model.User
import com.rainist.app.ui.main.repos.OnItemCallback

class StargazersViewHolder(
    view: View,
    viewPool: RecyclerView.RecycledViewPool,
    callback: OnItemCallback
) : RecyclerView.ViewHolder(view) {

    private val progressBar: ProgressBar = view.findViewById(R.id.progress_bar)
    private val recyclerView: RecyclerView = view.findViewById(R.id.recycler_view)
    private val emptyLabel: TextView = view.findViewById(R.id.label_empty)

    private val adapter = StargazersChildAdapter(callback)

    init {
        recyclerView.setRecycledViewPool(viewPool)
        recyclerView.adapter = adapter
        val itemAnimator = recyclerView.itemAnimator as SimpleItemAnimator
        itemAnimator.supportsChangeAnimations = false
    }

    fun bind(stargazers: List<User>?) {
        if (stargazers == null) {
            progressBar.visibility = VISIBLE
            recyclerView.visibility = GONE
            emptyLabel.visibility = GONE
            adapter.submitList(null)
        } else {
            progressBar.visibility = GONE
            recyclerView.visibility = VISIBLE
            emptyLabel.visibility = if (stargazers.isEmpty()) VISIBLE else GONE
            adapter.submitList(stargazers)
        }
    }
}