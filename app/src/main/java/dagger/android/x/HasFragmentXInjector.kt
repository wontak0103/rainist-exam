package dagger.android.x

import androidx.fragment.app.Fragment
import dagger.android.AndroidInjector
import dagger.internal.Beta

@Beta
interface HasFragmentXInjector {

    fun fragmentXInjector(): AndroidInjector<Fragment>
}
