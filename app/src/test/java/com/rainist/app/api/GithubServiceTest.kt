package com.rainist.app.api

import com.rainist.app.util.TestUtils
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsNull
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class GithubServiceTest {

    private lateinit var service: GithubService
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(GithubService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun `parse get repos response`() {
        TestUtils.enqueueMockResponse(mockWebServer, "get_repos_result.json")

        val testObserver = service.getRepos("googlesamples").test()
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()

        val repos = testObserver.values()[0]
        assertThat(repos, IsNull.notNullValue())
        assertThat(repos.size, `is`(30))

        assertThat(repos[0].id, `is`(66757521L))
        assertThat(repos[0].name, `is`("android-AccelerometerPlay"))
        assertThat(repos[0].desc, IsNull.nullValue())
        assertThat(repos[0].stars, `is`(108))

        assertThat(repos[9].id, `is`(43714189L))
        assertThat(repos[9].name, `is`("android-audio-high-performance"))
        assertThat(repos[9].desc, `is`("Code samples and documentation for building high performance audio apps on Android"))
        assertThat(repos[9].stars, `is`(688))

        assertThat(repos[29].id, `is`(41933021L))
        assertThat(repos[29].name, `is`("android-DirectShare"))
        assertThat(repos[29].desc, IsNull.nullValue())
        assertThat(repos[29].stars, `is`(102))
    }

    @Test
    fun `parse search stargazers response`() {
        TestUtils.enqueueMockResponse(mockWebServer, "get_repos_result.json")

        val testObserver = service.getRepos("googlesamples").test()
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()

        val repos = testObserver.values()[0]
        assertThat(repos, IsNull.notNullValue())
        assertThat(repos.size, `is`(30))

        assertThat(repos[0].id, `is`(66757521L))
        assertThat(repos[0].name, `is`("android-AccelerometerPlay"))
        assertThat(repos[0].desc, IsNull.nullValue())
        assertThat(repos[0].stars, `is`(108))

        assertThat(repos[9].id, `is`(43714189L))
        assertThat(repos[9].name, `is`("android-audio-high-performance"))
        assertThat(repos[9].desc, `is`("Code samples and documentation for building high performance audio apps on Android"))
        assertThat(repos[9].stars, `is`(688))

        assertThat(repos[29].id, `is`(41933021L))
        assertThat(repos[29].name, `is`("android-DirectShare"))
        assertThat(repos[29].desc, IsNull.nullValue())
        assertThat(repos[29].stars, `is`(102))
    }
}