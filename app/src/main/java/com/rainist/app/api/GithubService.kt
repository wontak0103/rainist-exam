package com.rainist.app.api

import com.rainist.app.api.model.Repo
import com.rainist.app.api.model.User
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubService {

    @GET("users/{username}")
    fun getUser(
        @Path("username") username: String
    ): Single<User>

    @GET("users/{username}/repos")
    fun getRepos(
        @Path("username") username: String,
        @Query("page") page: Int = 0,
        @Query("per_page") pageSize: Int = 30
    ): Single<List<Repo>>

    @GET("repos/{username}/{reponame}/stargazers")
    fun getStargazers(
        @Path("username") username: String,
        @Path("reponame") repoName: String
    ): Single<List<User>>

    companion object {

        const val BASE_URL = "https://api.github.com/"
    }
}