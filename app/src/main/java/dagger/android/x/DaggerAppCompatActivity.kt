package dagger.android.x

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.internal.Beta

import javax.inject.Inject

@Beta
abstract class DaggerAppCompatActivity : AppCompatActivity(), HasFragmentXInjector {

    @Inject lateinit var fragmentXInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun fragmentXInjector(): AndroidInjector<Fragment> {
        return fragmentXInjector
    }
}
