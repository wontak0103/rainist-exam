package com.rainist.app.ui.main.repos.stargazerstab

import android.util.ArrayMap
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rainist.app.R
import com.rainist.app.core.model.Repo
import com.rainist.app.core.model.User
import com.rainist.app.ui.extention.autoNotify
import com.rainist.app.ui.main.repos.OnItemCallback

class StargazersAdapter(
    private val callback: Callback
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface Callback : OnItemCallback {

        fun onRequestRepos(stargazerId: Long)
    }

    private val stargazers = arrayListOf<User>()
    private val stargazerIndexMap = ArrayMap<Long, Int>()
    private val starredReposInfo = ArrayMap<Long, List<Repo>>()
    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_STARGAZER -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.stargazer_vertical_view_item, parent, false)
                StargazerViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.repos_view_item, parent, false)
                ReposViewHolder(view, viewPool, callback)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val stargazer = stargazers[position / 2]
        when (holder) {
            is StargazerViewHolder -> {
                holder.bind(stargazer) {
                    val index = it / 2
                    callback.onItemClicked(
                        stargazers[index].avatarUrl,
                        stargazers[index].name
                    )
                }
            }
            is ReposViewHolder -> {
                val repos = starredReposInfo[stargazer.id]
                holder.bind(repos)
                if (!starredReposInfo.contains(stargazer.id)) {
                    starredReposInfo[stargazer.id] = null
                    callback.onRequestRepos(stargazer.id)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return stargazers.size * 2
    }

    override fun getItemViewType(position: Int): Int {
        return if (position % 2 == 0) VIEW_TYPE_STARGAZER else VIEW_TYPE_REPOS
    }

    fun submitStargazers(stargazers: List<User>) {
        autoNotify(this.stargazers, stargazers) { oldItem, newItem ->
            oldItem.id == newItem.id
        }

        this.stargazers.clear()
        this.stargazers.addAll(stargazers)
        buildStargazerIndexMap(stargazers)
    }

    fun submitStarredRepos(stargazerId: Long, repos: List<Repo>) {
        starredReposInfo[stargazerId] = repos
        stargazerIndexMap[stargazerId]?.let {
            notifyItemChanged(it * 2 + 1)
        }
    }

    private fun buildStargazerIndexMap(stargazers: List<User>) {
        stargazerIndexMap.clear()
        for (index in stargazers.indices) {
            stargazerIndexMap[stargazers[index].id] = index
        }
    }

    companion object {

        private const val VIEW_TYPE_STARGAZER = 0
        private const val VIEW_TYPE_REPOS = 1
    }
}