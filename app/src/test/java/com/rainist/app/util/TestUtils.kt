package com.rainist.app.util

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer

object TestUtils {

    fun enqueueMockResponse(mockWebServer: MockWebServer, fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader.getResourceAsStream("api-response/$fileName")
        val text = inputStream.bufferedReader().use { it.readText() }
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        mockWebServer.enqueue(
            mockResponse.setBody(text)
        )
    }
}