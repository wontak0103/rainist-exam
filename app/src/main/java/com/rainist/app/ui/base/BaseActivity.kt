package com.rainist.app.ui.base

import dagger.android.x.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity()