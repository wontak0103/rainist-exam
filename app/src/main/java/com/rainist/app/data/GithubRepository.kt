package com.rainist.app.data

import android.util.Log
import com.rainist.app.api.GithubService
import com.rainist.app.api.toCoreModel
import com.rainist.app.core.model.Repo
import com.rainist.app.core.model.Star
import com.rainist.app.core.model.User
import com.rainist.app.db.dao.RepoDao
import com.rainist.app.db.dao.StarDao
import com.rainist.app.db.dao.UserDao
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GithubRepository @Inject constructor(
    private val service: GithubService,
    private val repoDao: RepoDao,
    private val userDao: UserDao,
    private val starDao: StarDao
) {

    fun getUser(username: String): Single<User> {
        return service.getUser(username)
            .map { it.toCoreModel() }
            .flatMap { userDao.insert(it).toSingleDefault(it) }
            .onErrorResumeNext { userDao.findUserByName(username).map { it[0] } }
    }

    fun getRepos(userId: Long, page: Int, pageSize: Int): Single<List<Repo>> {
        return userDao.user(userId)
            .onErrorResumeNext { userDao.user(userId) }
            .flatMap { user ->
                if (user.repoCount == 0) {
                    Single.just(emptyList())
                } else {
                    val offset = page * pageSize
                    if (user.repoCount < offset) {
                        Single.just(emptyList())
                    } else {
                        repoDao.reposByOwnerId(user.id, pageSize, offset)
                            .flatMap { repos ->
                                if (repos.isEmpty()) {
                                    getReposFromServer(user.name, page, pageSize)
                                        .flatMap { saveReposInLocalStorage(it).toSingleDefault(it) }
                                } else {
                                    Single.just(repos)
                                }
                            }
                    }
                }
            }
    }

    fun getRepoIdsFromLocal(username: String): Single<List<Long>> {
        return userDao.findUserByName(username)
            .flatMap { users ->
                if (users.isEmpty()) {
                    Single.just(emptyList())
                } else {
                    repoDao.repoIdsByOwnerId(users[0].id)
                }
            }
    }

    fun getReposFromServer(username: String, page: Int, pageSize: Int): Single<List<Repo>> {
        return service.getRepos(username, page, pageSize)
            .flatMap { repos ->
                Observable.fromIterable(repos)
                    .map { it.toCoreModel() }
                    .toList()
            }
    }

    fun getRepo(repoId: Long): Single<Repo> {
        return repoDao.repo(repoId)
    }

    fun getStarredRepos(stargazerId: Long): Single<List<Repo>> {
        return starDao.starredRepos(stargazerId)
            .flatMap { repoIds ->
                if (repoIds.isEmpty()) {
                    Single.just(emptyList())
                } else {
                    repoDao.repos(repoIds)
                }
            }
    }

    fun getStargazers(username: String, repoId: Long): Single<List<User>> {
        return repoDao.repo(repoId)
            .flatMap { repo ->
                if (repo.stars == 0) {
                    Single.just(emptyList())
                } else {
                    starDao.stargazers(repo.id)
                        .flatMap { stargazerIds ->
                            if (stargazerIds.isEmpty()) {
                                Log.d("GithubRepository", "Get ${repo.name}'s users from server")
                                service.getStargazers(username, repo.name)
                                    .flatMap { users ->
                                        Observable.fromIterable(users)
                                            .map { it.toCoreModel() }
                                            .toList()
                                    }
                                    .flatMap { stargazers ->
                                        saveStargazersInLocalStorage(repo.id, stargazers)
                                            .toSingleDefault(stargazers)
                                    }
                            } else {
                                Log.d("GithubRepository", "$repoId's stargazer found")
                                userDao.users(stargazerIds)
                            }
                        }
                }
            }
    }

    fun getStargazersFromLocal(repoIds: List<Long>): Single<List<User>> {
        return starDao.stargazers(repoIds)
            .flatMap { userIds ->
                Observable.fromIterable(userIds.toSet().chunked(LIST_PARAMS_CHUNK_SIZE))
                    .concatMap { userDao.users(it).toObservable() }
                    .concatMapIterable { it }
                    .toList()
            }
    }

    private fun saveReposInLocalStorage(repos: List<Repo>): Completable {
        return repoDao.insert(repos)
    }

    private fun saveStargazersInLocalStorage(repoId: Long, stargazers: List<User>): Completable {
        val stars = arrayListOf<Star>()
        for (stargazer in stargazers) {
            stars.add(Star(0, repoId, stargazer.id))
        }
        return starDao.insert(stars)
            .andThen(saveUsersInLocalStorage(stargazers))
    }

    private fun saveUsersInLocalStorage(users: List<User>): Completable {
        return userDao.insert(users)
    }

    companion object {

        const val LIST_PARAMS_CHUNK_SIZE = 999
    }
}