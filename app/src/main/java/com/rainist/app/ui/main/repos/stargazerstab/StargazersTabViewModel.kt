package com.rainist.app.ui.main.repos.stargazerstab

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rainist.app.core.model.Repo
import com.rainist.app.core.model.User
import com.rainist.app.data.GithubRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class StargazersTabViewModel @Inject constructor(
    private val repository: GithubRepository
) : ViewModel() {

    val stargazers = MutableLiveData<List<User>>()
    val starredRepos = MutableLiveData<Pair<Long, List<Repo>>>()

    private val compositeDisposable = CompositeDisposable()

    fun loadStargazers(username: String) {
        val disposable = repository.getRepoIdsFromLocal(username)
            .flatMap { repository.getStargazersFromLocal(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                stargazers.value = it
            }, {
                it.printStackTrace()
            })
        compositeDisposable.add(disposable)
    }

    fun getStarredRepos(stargazerId: Long) {
        val disposable = repository.getStarredRepos(stargazerId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ starredRepos.value = Pair(stargazerId, it) }, { it.printStackTrace() })
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}