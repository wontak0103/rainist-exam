package com.rainist.app.api.model

import com.google.gson.annotations.SerializedName

data class Repo(
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val desc: String?,
    @SerializedName("stargazers_count")
    val stars: Int,
    @SerializedName("owner")
    val owner: User
)