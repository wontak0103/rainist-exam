package com.rainist.app.core.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "stars")
data class Star(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Long,
    @ColumnInfo(name = "repo_id")
    val repoId: Long,
    @ColumnInfo(name = "user_id")
    val userId: Long
)