package com.rainist.app.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rainist.app.core.model.Star
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface StarDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(relationships: List<Star>): Completable

    @Query("SELECT user_id FROM stars WHERE repo_id = :repoId")
    fun stargazers(repoId: Long): Single<List<Long>>

    @Query("SELECT user_id FROM stars WHERE repo_id IN (:repoIds)")
    fun stargazers(repoIds: List<Long>): Single<List<Long>>

    @Query("SELECT repo_id FROM stars WHERE user_id = :userId")
    fun starredRepos(userId: Long): Single<List<Long>>
}