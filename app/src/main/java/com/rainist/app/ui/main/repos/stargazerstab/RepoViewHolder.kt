package com.rainist.app.ui.main.repos.stargazerstab

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rainist.app.R
import com.rainist.app.core.model.Repo
import com.squareup.picasso.Picasso

class RepoViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val image: ImageView = view.findViewById(R.id.image)
    private val nameLabel: TextView = view.findViewById(R.id.label_name)

    fun bind(repo: Repo?, onClick: (position: Int) -> Unit) {
        itemView.setOnClickListener {
            onClick(adapterPosition)
        }
        repo?.let {
            Picasso.get()
                .load(repo.ownerAvatarUrl)
                .into(image)
            nameLabel.text = repo.name
        }
    }
}