package com.rainist.app.ui.main.repos

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.rainist.app.ui.main.repos.repostab.ReposTabFragment
import com.rainist.app.ui.main.repos.stargazerstab.StargazersTabFragment

class ReposPagerAdapter(
    username: String,
    fm: FragmentManager
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val fragments = ArrayList<Fragment>()
    private val fragmentTitles = ArrayList<String>()

    init {
        fragments.add(ReposTabFragment.newInstance(username))
        fragmentTitles.add("Repos")

        fragments.add(StargazersTabFragment.newInstance(username))
        fragmentTitles.add("Stargazers")
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentTitles[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }
}