package com.rainist.app.ui.main.repos.repostab

import android.util.ArrayMap
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rainist.app.R
import com.rainist.app.core.model.Repo
import com.rainist.app.core.model.User
import com.rainist.app.ui.extention.autoNotify
import com.rainist.app.ui.main.repos.OnItemCallback

class ReposAdapter(
    private val callback: Callback
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface Callback : OnItemCallback {

        fun onRequestStargazers(repoId: Long)
    }

    private val repos = arrayListOf<Repo>()
    private val repoIndexMap = ArrayMap<Long, Int>()
    private val stargazersInfo = ArrayMap<Long, List<User>>()
    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_REPO -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.repo_vertical_view_item, parent, false)
                RepoViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.stargazers_view_item, parent, false)
                StargazersViewHolder(view, viewPool, callback)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val repo = repos[position / 2]
        when (holder) {
            is RepoViewHolder -> {
                holder.bind(repo) {
                    val index = it / 2
                    callback.onItemClicked(
                        repos[index].ownerAvatarUrl,
                        repos[index].name
                    )
                }
            }
            is StargazersViewHolder -> {
                val stargazers = stargazersInfo[repo.id]
                if (!stargazersInfo.contains(repo.id)) {
                    stargazersInfo[repo.id] = null
                    callback.onRequestStargazers(repo.id)
                }
                holder.bind(stargazers)
            }
        }
    }

    override fun getItemCount(): Int {
        return repos.size * 2
    }

    override fun getItemViewType(position: Int): Int {
        return if (position % 2 == 0) VIEW_TYPE_REPO else VIEW_TYPE_STARGAZERS
    }

    fun submitRepos(repos: List<Repo>) {
        autoNotify(this.repos, repos) { oldItem, newItem ->
            oldItem.id == newItem.id
        }

        this.repos.clear()
        this.repos.addAll(repos)
        buildRepoIndexMap()
    }

    fun addMoreRepos(repos: List<Repo>) {
        autoNotify(this.repos, repos) { oldItem, newItem ->
            oldItem.id == newItem.id
        }

        this.repos.addAll(repos)
        buildRepoIndexMap()
    }

    fun submitStargazers(repoId: Long, stargazers: List<User>) {
        stargazersInfo[repoId] = stargazers
        repoIndexMap[repoId]?.let {
            notifyItemChanged(it * 2 + 1)
        }
    }

    private fun buildRepoIndexMap() {
        repoIndexMap.clear()
        for (index in repos.indices) {
            repoIndexMap[repos[index].id] = index
        }
    }

    companion object {

        private const val VIEW_TYPE_REPO = 0
        private const val VIEW_TYPE_STARGAZERS = 1
    }
}