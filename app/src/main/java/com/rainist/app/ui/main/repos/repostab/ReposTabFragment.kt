package com.rainist.app.ui.main.repos.repostab

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import com.rainist.app.R
import com.rainist.app.core.model.Repo
import com.rainist.app.core.model.User
import com.rainist.app.ui.base.BaseFragment
import com.rainist.app.ui.main.repos.OnItemCallback
import kotlinx.android.synthetic.main.repos_tab_fragment.*
import javax.inject.Inject

class ReposTabFragment : BaseFragment(), ReposAdapter.Callback {

    @Inject
    lateinit var viewModel: ReposTabViewModel

    private val adapter = ReposAdapter(this)
    private lateinit var callback: OnItemCallback

    private var userId: Long = 0L
    private var maxRepoCount = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.repos_tab_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initViewModel()
        viewModel.getUser(arguments?.getString(KEY_USERNAME)!!)
    }

    private fun initRecyclerView() {
        recycler_view.adapter = adapter
        recycler_view.addOnScrollListener(object : RecyclerViewPaginator(recycler_view, PAGE_SIZE) {
            override fun loadMore(currentPageIndex: Int, nextPageIndex: Int, pageSize: Int) {
                if (currentPageIndex * pageSize + pageSize > maxRepoCount) {
                    Log.d("ReposTabFragment", "Reached end of pages")
                } else {
                    Log.d("ReposTabFragment", "Add more repos")
                    viewModel.addMoreRepos(userId, nextPageIndex, pageSize)
                }
            }
        })
    }

    private fun initViewModel() {
        viewModel.user.observe(this, Observer<User> {
            maxRepoCount = it.repoCount
            userId = it.id
            viewModel.getRepos(it.id, 0, PAGE_SIZE)
        })
        viewModel.repos.observe(this, Observer<List<Repo>> {
            if (it.isEmpty()) {
                Toast.makeText(context, "Couldn't find any repos : ${arguments?.getString(KEY_USERNAME)}", LENGTH_SHORT).show()
            } else {
                adapter.submitRepos(it)
            }
        })
        viewModel.moreRepos.observe(this, Observer<List<Repo>> {
            if (it.isEmpty()) {
                Toast.makeText(context, "Couldn't find any repos : ${arguments?.getString(KEY_USERNAME)}", LENGTH_SHORT).show()
            } else {
                adapter.addMoreRepos(it)
            }
        })
        viewModel.stargazers.observe(this, Observer<Pair<Long, List<User>>> {
            Log.d("ReposTabFragment", "Show users - ${it.first} :: ${it.second.size}")
            adapter.submitStargazers(it.first, it.second)
        })
        viewModel.error.observe(this, Observer<String> {
            Toast.makeText(context, it, LENGTH_SHORT).show()
        })
    }

    override fun onRequestStargazers(repoId: Long) {
        Log.d("ReposTabFragment", "On Request Stargazers : $repoId")
        viewModel.getStargazers(arguments?.getString(KEY_USERNAME)!!, repoId)
    }

    override fun onItemClicked(avatarUrl: String, name: String) {
        callback.onItemClicked(avatarUrl, name)
    }

    fun setCallback(callback: OnItemCallback) {
        this.callback = callback
    }

    companion object {

        const val KEY_USERNAME = "username"
        private const val PAGE_SIZE = 30

        fun newInstance(username: String): ReposTabFragment {
            return ReposTabFragment().apply {
                arguments = bundleOf(Pair(KEY_USERNAME, username))
            }
        }
    }
}