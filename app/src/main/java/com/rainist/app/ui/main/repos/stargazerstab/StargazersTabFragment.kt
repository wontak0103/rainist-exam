package com.rainist.app.ui.main.repos.stargazerstab

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import com.rainist.app.R
import com.rainist.app.core.model.Repo
import com.rainist.app.core.model.User
import com.rainist.app.ui.base.BaseFragment
import com.rainist.app.ui.main.repos.OnItemCallback
import kotlinx.android.synthetic.main.stargazers_tab_fragment.*
import javax.inject.Inject

class StargazersTabFragment : BaseFragment(), StargazersAdapter.Callback {

    @Inject
    lateinit var viewModel: StargazersTabViewModel

    private val adapter = StargazersAdapter(this)
    private var callback: OnItemCallback? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.stargazers_tab_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_view.adapter = adapter
        initViewModel()
    }

    private fun initViewModel() {
        viewModel.stargazers.observe(this, Observer<List<User>> {
            adapter.submitStargazers(it)
        })
        viewModel.starredRepos.observe(this, Observer<Pair<Long, List<Repo>>> {
            Log.d("StargazersTabFragment", "Show repos - ${it.first} :: ${it.second.size}")
            adapter.submitStarredRepos(it.first, it.second)
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadStargazers(arguments?.getString(KEY_USERNAME)!!)
    }

    override fun onRequestRepos(stargazerId: Long) {
        Log.d("StargazersTabFragment", "On Request Repos : $stargazerId")
        viewModel.getStarredRepos(stargazerId)
    }

    override fun onItemClicked(avatarUrl: String, name: String) {
        callback?.onItemClicked(avatarUrl, name)
    }

    fun setCallback(callback: OnItemCallback) {
        this.callback = callback
    }

    companion object {

        const val KEY_USERNAME = "username"

        fun newInstance(username: String): StargazersTabFragment {
            return StargazersTabFragment().apply {
                arguments = bundleOf(Pair(KEY_USERNAME, username))
            }
        }
    }
}