package com.rainist.app.ui.main

import com.rainist.app.di.FragmentScoped
import com.rainist.app.ui.main.details.DetailsFragment
import com.rainist.app.ui.main.repos.ReposFragment
import com.rainist.app.ui.main.repos.repostab.ReposTabFragment
import com.rainist.app.ui.main.repos.stargazerstab.StargazersTabFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun contributeDetailsFragment(): DetailsFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun contributeReposFragment(): ReposFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun contributeReposTabFragment(): ReposTabFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun contributeStargazersTabFragment(): StargazersTabFragment
}