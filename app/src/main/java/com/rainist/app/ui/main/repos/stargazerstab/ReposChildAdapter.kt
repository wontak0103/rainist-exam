package com.rainist.app.ui.main.repos.stargazerstab

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.rainist.app.R
import com.rainist.app.core.model.Repo
import com.rainist.app.ui.main.repos.OnItemCallback

class ReposChildAdapter(
    private val callback: OnItemCallback
) : ListAdapter<Repo, RepoViewHolder>(COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.repo_horizontal_view_item, parent, false)
        return RepoViewHolder(view)
    }

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        val repo = getItem(position)
        holder.bind(repo) {
            val item = getItem(it)
            callback.onItemClicked(item.ownerAvatarUrl, item.name)
        }
    }

    companion object {

        private val COMPARATOR = object : DiffUtil.ItemCallback<Repo>() {
            override fun areItemsTheSame(oldItem: Repo, newItem: Repo): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Repo, newItem: Repo): Boolean =
                oldItem == newItem
        }
    }
}