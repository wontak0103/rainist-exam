package dagger.android.x

import android.content.Context
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.internal.Beta
import javax.inject.Inject

@Beta
abstract class DaggerDialogFragment : DialogFragment(), HasFragmentXInjector {

    @Inject lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onAttach(context: Context) {
        AndroidXInjection.inject(this)
        super.onAttach(context)
    }

    override fun fragmentXInjector(): AndroidInjector<Fragment> {
        return childFragmentInjector
    }
}
