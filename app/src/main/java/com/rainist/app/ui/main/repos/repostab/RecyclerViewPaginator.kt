package com.rainist.app.ui.main.repos.repostab

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE

abstract class RecyclerViewPaginator(
    recyclerView: RecyclerView,
    private val pageSize: Int
) : RecyclerView.OnScrollListener() {

    private var isLoading = false
    private var totalItemCount = 0
    private val layoutManager = recyclerView.layoutManager!!

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (newState == SCROLL_STATE_IDLE) {
            val firstVisibleItemPosition = when (layoutManager) {
                is LinearLayoutManager -> {
                    layoutManager.findFirstVisibleItemPosition()
                }
                is GridLayoutManager -> {
                    layoutManager.findFirstVisibleItemPosition()
                }
                else -> {
                    0
                }
            }
            val visibleItemCount = layoutManager.childCount

            if (isLoading && totalItemCount == layoutManager.itemCount) {
                return
            }

            totalItemCount = layoutManager.itemCount

            if (firstVisibleItemPosition + visibleItemCount >= totalItemCount) {
                val currentPageIndex = totalItemCount / pageSize
                val nextPageIndex = currentPageIndex + 1
                loadMore(currentPageIndex, nextPageIndex, pageSize)
                isLoading = true
            }
        }
    }

    abstract fun loadMore(currentPageIndex: Int, nextPageIndex: Int, pageSize: Int)
}