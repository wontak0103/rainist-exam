package com.rainist.app.ui.main.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.rainist.app.R
import com.rainist.app.core.model.Repo
import com.rainist.app.ui.base.BaseFragment
import com.rainist.app.ui.main.MainActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.details_fragment.*
import javax.inject.Inject

class DetailsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: DetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.details_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        container_main.setOnClickListener {
            if (label_name.text.isNotEmpty()) {
                val intent = MainActivity.createIntent(context!!, label_name.text.toString())
                startActivity(intent)
            }
        }
        initViewModel()
    }

    private fun initViewModel() {
        viewModel.repo.observe(this, Observer<Repo> {
            Picasso.get()
                .load(it.ownerAvatarUrl)
                .into(image)
            label_name.text = it.name
        })
    }

    fun show(avatarUrl: String, name: String) {
        Picasso.get()
            .load(avatarUrl)
            .into(image)
        label_name.text = name
    }


    companion object {

        const val TAG = "DetailsFragment"
    }
}