package dagger.android.x

import androidx.fragment.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.internal.Beta

import javax.inject.Inject

@Beta
abstract class DaggerApplication : dagger.android.DaggerApplication(), HasFragmentXInjector {

    @Inject
    lateinit var fragmentXInjector: DispatchingAndroidInjector<Fragment>

    abstract override fun applicationInjector(): AndroidInjector<out DaggerApplication>

    override fun fragmentXInjector(): DispatchingAndroidInjector<Fragment> {
        return fragmentXInjector
    }
}
