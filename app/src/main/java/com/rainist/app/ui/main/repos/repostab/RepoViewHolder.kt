package com.rainist.app.ui.main.repos.repostab

import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.rainist.app.R
import com.rainist.app.core.model.Repo

class RepoViewHolder(
    view: View
) : RecyclerView.ViewHolder(view) {

    private val nameLabel: TextView = view.findViewById(R.id.label_name)
    private val descLabel: TextView = view.findViewById(R.id.label_desc)
    private val starsLabel: TextView = view.findViewById(R.id.label_stars)

    private var repo: Repo? = null

    fun bind(repo: Repo?, onClick: (position: Int) -> Unit) {
        itemView.setOnClickListener {
            onClick(adapterPosition)
        }
        repo?.let {
            this.repo = repo
            nameLabel.text = repo.name
            descLabel.text = if (repo.desc.isNullOrEmpty()) "Empty" else repo.desc
            starsLabel.text = repo.stars.toString()
            val starsTextColor = if (repo.stars > 50) Color.RED else
                ContextCompat.getColor(itemView.context, R.color.primary_text_color)
            starsLabel.setTextColor(starsTextColor)
        }
    }
}