package com.rainist.app.di

import androidx.room.Room
import com.rainist.app.BuildConfig
import com.rainist.app.MobileApplication
import com.rainist.app.api.GithubService
import com.rainist.app.db.AppDatabase
import com.rainist.app.db.dao.RepoDao
import com.rainist.app.db.dao.StarDao
import com.rainist.app.db.dao.UserDao
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor)
        }

        return builder.build()
    }

    @Singleton
    @Provides
    fun provideGithubService(client: OkHttpClient): GithubService {
        return Retrofit.Builder()
            .baseUrl(GithubService.BASE_URL)
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(GithubService::class.java)
    }

    @Singleton
    @Provides
    fun provideAppDatabase(app: MobileApplication): AppDatabase {
        return Room.databaseBuilder(app,
            AppDatabase::class.java, "app.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideRepoDao(database: AppDatabase): RepoDao {
        return database.reposDao()
    }

    @Singleton
    @Provides
    fun provideStargazerDao(database: AppDatabase): UserDao {
        return database.userDao()
    }

    @Singleton
    @Provides
    fun provideStargazerRelationshipDao(database: AppDatabase): StarDao {
        return database.stargazerDao()
    }
}