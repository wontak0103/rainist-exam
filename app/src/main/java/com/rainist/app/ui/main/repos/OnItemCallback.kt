package com.rainist.app.ui.main.repos

interface OnItemCallback {

    fun onItemClicked(avatarUrl: String, name: String)
}