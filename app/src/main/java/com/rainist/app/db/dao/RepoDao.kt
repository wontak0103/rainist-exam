package com.rainist.app.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rainist.app.core.model.Repo
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface RepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(repos: List<Repo>): Completable

    @Query("SELECT * FROM repos WHERE id = :id LIMIT 1")
    fun repo(id: Long): Single<Repo>

    @Query("SELECT * FROM repos WHERE id IN (:ids) ORDER BY name ASC")
    fun repos(ids: List<Long>): Single<List<Repo>>

    @Query("SELECT COUNT(*) FROM repos")
    fun repoCount(): Single<Int>

    @Query("SELECT * FROM repos WHERE owner_id = :ownerId ORDER BY name ASC")
    fun reposByOwnerId(ownerId: Long): Single<List<Repo>>

    @Query("SELECT * FROM repos WHERE owner_id = :ownerId ORDER BY name ASC LIMIT :limit OFFSET :offset")
    fun reposByOwnerId(ownerId: Long, limit: Int, offset: Int): Single<List<Repo>>

    @Query("SELECT id FROM repos WHERE owner_id = :ownerId")
    fun repoIdsByOwnerId(ownerId: Long): Single<List<Long>>

    @Query("SELECT COUNT(*) FROM repos WHERE owner_id = :ownerId ORDER BY name ASC")
    fun repoCountByOwnerId(ownerId: Long): Single<Int>

}
