package com.rainist.app.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import com.rainist.app.R
import com.rainist.app.ui.base.BaseActivity
import com.rainist.app.ui.main.details.DetailsFragment
import com.rainist.app.ui.main.repos.OnItemCallback
import com.rainist.app.ui.main.repos.ReposFragment
import com.rainist.app.ui.main.repos.repostab.ReposTabFragment
import com.rainist.app.ui.main.repos.stargazerstab.StargazersTabFragment

class MainActivity : BaseActivity(), OnItemCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        initFragments()
    }

    private fun initFragments() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container_details, DetailsFragment(), DetailsFragment.TAG)
            .replace(R.id.container_repos, ReposFragment.newInstance(getTargetUsername()))
            .commit()
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)
        when (fragment) {
            is ReposTabFragment -> {
                fragment.setCallback(this)
            }
            is StargazersTabFragment -> {
                fragment.setCallback(this)
            }
        }
    }

    override fun onItemClicked(avatarUrl: String, name: String) {
        Log.d("MainActivity", "On item clicked : $name")
        val detailsFragment = supportFragmentManager.findFragmentByTag(DetailsFragment.TAG) as DetailsFragment
        detailsFragment.show(avatarUrl, name)
    }

    private fun getTargetUsername(): String {
        var username = intent.getStringExtra(EXTRA_USERNAME)
        if (username.isNullOrEmpty()) {
            username = DEFAULT_USERNAME
        }
        return username
    }

    companion object {

        private const val DEFAULT_USERNAME = "googlesamples"
        private const val EXTRA_USERNAME = "username"

        fun createIntent(context: Context, username: String): Intent {
            return Intent(context, MainActivity::class.java).apply {
                putExtra(EXTRA_USERNAME, username)
            }
        }
    }
}
