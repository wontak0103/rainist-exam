package com.rainist.app.ui.main.repos.repostab

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rainist.app.core.model.Repo
import com.rainist.app.core.model.User
import com.rainist.app.data.GithubRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ReposTabViewModel @Inject constructor(
    private val repository: GithubRepository
) : ViewModel() {

    val user = MutableLiveData<User>()
    val repos = MutableLiveData<List<Repo>>()
    val moreRepos = MutableLiveData<List<Repo>>()
    val stargazers = MutableLiveData<Pair<Long, List<User>>>()
    val error = MutableLiveData<String>()

    private val compositeDisposable = CompositeDisposable()

    fun getUser(username: String) {
        val disposable = repository.getUser(username)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ user.value = it }, { error.value = it.message })
        compositeDisposable.add(disposable)
    }

    fun getRepos(userId: Long, page: Int, pageSize: Int) {
        val disposable = repository.getRepos(userId, page, pageSize)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ repos.value = it}, { error.value = it.message })
        compositeDisposable.add(disposable)
    }

    fun addMoreRepos(userId: Long, page: Int, pageSize: Int) {
        val disposable = repository.getRepos(userId, page, pageSize)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ moreRepos.value = it}, { error.value = it.message })
        compositeDisposable.add(disposable)
    }

    fun getStargazers(username: String, id: Long) {
        val disposable = repository.getStargazers(username, id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ stargazers.value = Pair(id, it) }, { error.value = it.message })
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}